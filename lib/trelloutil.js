var trelloOptions = {
    url:'KnA4E3zK',//'jzageE8R',
    key:'60bb1bf9ca6ee2f8487a6211a4f3e379',
    token:'34171291f1856139e01f2fb2b1c35653c54b83a0dcf85eb619cff6c1776a334e',
    board:{
      id:''
    },
    lists:{
      a:[]
    }
  }
  
  //переводит кол-во дней в милисекунды для работы с объектом Date
  function msecToDays(msec) {
    return (msec/(24*60*60*1000)>>0);//возвращаю целое число
  }
  
  //Теперь добавлю информацию в массив Карт
  //   1. дата размещения карты (закодирован в ID карты)
  //   2. срок исполнения "due"
  //   3. Просрочка (значение с Минусом) или Сколько осталось (Значение с Плюсом)
  function GetDatesTimesOfCards(a){
    var i = a.length;
    while (i-- !=0) {
      let o = a[i];
      //Дата создания карты запакована в  его id
      o.createDate = new Date(1000*parseInt(o.id.substring(0,8),16)).getTime();
      let due = new Date(o.due).getTime();
      let now = Date.now();
      //Просрочка (expiration) это разность времени между
      //Сегодня и Дедлайном (o.due), которое надо выразить в днях
      o.daysOfExpiration = msecToDays(now - due);
      //Ещё один параметр, это сколько дней Заказ находится в работе
      //от даты размещения до сегодня 
      o.daysInProgress =  msecToDays(now - o.createDate);
      //И ещё один параметр, сколько дней Заказ ДОЛЖЕН был выполняться
      //это разность между Дедлайном и Временем размещения
      o.daysOfNormalProgress = msecToDays(due - o.createDate);
    }
    console.log(a);
  }
  
  //в массиве Списков, ищет список с заданным именем и возвращает его ID
  function GetListIDByName(name, a){
    var i = a.length;
    while (i-- !=0) {
      let o = a[i];
      if (o.name == name) return o.id;
    }
    return null;
  }
  //получает Карты конкретного Списка по его ID
  function GetCardsOfList (id ,trelloOptions) {
    var res = undefined; //результат
    var o = trelloOptions;
                    //https://api.trello.com/1/lists/id/cards
                    //"https://api.trello.com/1/boards/jzageE8R/cards?key=60bb1bf9ca6ee2f8487a6211a4f3e379&token=34171291f1856139e01f2fb2b1c35653c54b83a0dcf85eb619cff6c1776a334e");
    res = getDataSync('https://api.trello.com/1/lists/'+id+'/cards?key='+o.key+'&token='+o.token);
    return res;
  }
  
  function GetBoardID (trelloOptions) {
    var res = undefined; //результат
    var o = trelloOptions;
    res = getDataSync('https://api.trello.com/1/boards/'+o.url+'?key='+o.key+'&token='+o.token);
    return res;
  }
  
  function GetAllLists (trelloOptions) {
    var res = undefined; //результат
    var o = trelloOptions;
                    //"https://api.trello.com/1/boards/jzageE8R/lists?key=60bb1bf9ca6ee2f8487a6211a4f3e379&token=34171291f1856139e01f2fb2b1c35653c54b83a0dcf85eb619cff6c1776a334e");
    res = getDataSync('https://api.trello.com/1/boards/'+o.url+'/lists?key='+o.key+'&token='+o.token);
    return res;
  }
  
  function GetAllCards (trelloOptions) {
    var res = undefined; //результат
    var o = trelloOptions;
                    //"https://api.trello.com/1/boards/jzageE8R/cards?key=60bb1bf9ca6ee2f8487a6211a4f3e379&token=34171291f1856139e01f2fb2b1c35653c54b83a0dcf85eb619cff6c1776a334e");
    res = getDataSync('https://api.trello.com/1/boards/'+o.url+'/cards?key='+o.key+'&token='+o.token);
    return res;
  }
  
  function getDataSync(option) {
    var obj = undefined;
    var req = new XMLHttpRequest();
        req.open("GET", option, false);//false - делаю сихронный запрос к http-серверу 
        req.send();
        if (req.status !=200) {//ошибка
            console.log(req.status+':'+req.statusText);
        }
        else {//получил данные
            obj =  JSON.parse(req.responseText); 
            console.log(obj);
        }
    return obj;
  }