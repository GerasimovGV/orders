Vue.component('process-diagram', {
    props: {
      //инфа из карты (её потом парсить)
      orderInfo:Object,
      index:Number
    },
    data: function () {
      return {
        //исходные данные для масштаба
        //this.orderInfo.totalDivsSize,//задаётся в % суммарная ширина div-блоков для Прогресса и Просрочек
        //this.orderInfo.maxExpiration, //максимальная просрочка (из всех заказов)
        //this.orderInfo.maxProgress, //самый длинный по времени заказ
        //данные конкретного заказа
        //this.orderInfo.daysInProgress,      //продолжительность выполнения заказа
        //this.orderInfo.daysOfExpiration,    //дней просрочки
        //this.orderInfo.daysOfNormalProgress, //разность между Дедлайном и Временем размещения
        beforeDueDays:this.orderInfo.daysOfNormalProgress - this.orderInfo.daysInProgress,//осталось дней до дедлайна
        //Стили:
        //1) DIV-ы - максимальные индикаторы (определяющие масштабы для всех остальных заказов)
        MaxProgressStyle: {//заказ с самым длинным заданным сроком изготовления
          //вычисляемые свойства
          //maxWidth: '15%',
          //minWidth: '15%',
          //backgroundColor: 'lightgray'
        },
        MaxExpirationStyle: {//максимально просроченный заказ
          //вычисляемые свойства
          //maxWidth: '10%',
          //minWidth: '10%',
          //backgroundColor: 'gray'
        },
        //DIV-ы - индикаторы конкретного проекта
        NormalProgressStyle: {//нормальное время выполнения проекта
          position: "absolute",
          top: "0%",
          //вычисляемые свойства
          //left: "40%",
          //width: "60%",
          border: "1px outset gray",
          height: "auto",
          boxSizing: "border-box"
        },
        ProgressStyle: {//текущее время выполнения проекта
          position: "absolute",
          top: "0%",
          left: "0%",
          //вычисляемые свойства
          //width: "30%",
          //backgroundColor: "yellowgreen"
          height:"100%",
          boxSizing: "border-box"
        },
        BeforeDueDaysStyle: {//дней до дедлайна
          position: "absolute",
          top: "0%",
          //вычисляемые свойства
          //width: "30%",
          //left: "0%",
          backgroundColor: "yellowgreen",
          height:"100%",
          boxSizing: "border-box",
        },
        ExpirationStyle: {//просрочка
          position: "absolute",
          top: "0%",
          left: "0%",
          //width: "30%", - вычисляемое свойство
          backgroundColor: "red",
          border: "1px outset gray",
          //height:"100%",
          height: "auto",
          boxSizing: "border-box",
          color:"White"
        }
      }
    },
    computed: {
        //Блоки макс исполнения и макс-й просрочки
        getMaxProgressStyle: function (){
            this.MaxProgressStyle.maxWidth = 
            this.MaxProgressStyle.minWidth = this.getDivsSizes().progressWidth;
            return this.MaxProgressStyle;  
        }, 

        getMaxExpirationStyle: function (){
            this.MaxExpirationStyle.maxWidth = 
            this.MaxExpirationStyle.minWidth = this.getDivsSizes().expirationWidth;
            return this.MaxExpirationStyle;  
        }, 
        
        //Диаграммы нормального исполнения и просрочки
        getNormalProgressStyle: function (){
            var x = (this.orderInfo.daysOfNormalProgress * 100)/this.orderInfo.maxProgress;
            if (x > 100) x = 100;
              this.NormalProgressStyle.left = (100.0-x)+"%";
              this.NormalProgressStyle.width = x+"%";
            return this.NormalProgressStyle;
        },

        getProgressStyle: function () {
            //цвет прогресс-бара в зависимости от просрочен/не просрочен заказ
            if (this.orderInfo.daysOfExpiration > 0) {
              this.ProgressStyle.color = "black";
              this.ProgressStyle.backgroundColor = "red";
            }
            else {
              //добавлю через / сколько дней осталось на выполнение заказа
              this.ProgressStyle.color = "gray";
              this.ProgressStyle.backgroundColor = "lightgray";
            }
            //Длина прогресс-бара, пропорциональна дням выполнения daysInProgress
            //daysOfNormalProgress - это 100%
            //daysInProgress - х% => X% = (daysInProgress * 100%)/ daysOfNormalProgress;
            //Но не больше 100%!
            var x = (this.orderInfo.daysInProgress * 100)/this.orderInfo.daysOfNormalProgress;
            if (x > 100) x = 100;
            if (x <   0) x =   0;
            this.ProgressStyle.width = x+"%";
            return this.ProgressStyle;
        },

        getBeforeDueDays: function (){
            this.beforeDueDays = this.orderInfo.daysOfNormalProgress - this.orderInfo.daysInProgress;
            var x = (this.beforeDueDays * 100)/this.orderInfo.daysOfNormalProgress;
            if (x > 100) x = 100;
            if (x <   0) {
              x =   0;
              this.beforeDueDays = '';
            }
            this.BeforeDueDaysStyle.left = (100 - x)+"%";
            this.BeforeDueDaysStyle.width = x+"%";
            return this.BeforeDueDaysStyle;
        },

        getExpirationStyle: function (){
            //определяю длину прогресс бара daysOfExpiration
            //maxExpiration - это 100%
            //daysOfExpiration - х% => X% = (daysOfExpiration * 100%)/ maxExpiration;
            //Но не больше 100%!
            var x = (this.orderInfo.daysOfExpiration * 100)/this.orderInfo.maxExpiration
            if (x > 100) x = 100;
            if (x <= 0)  x = 0;
            this.ExpirationStyle.maxWidth = 
              this.ExpirationStyle.minWidth = 
                this.ExpirationStyle.width = x+"%";
            return this.ExpirationStyle;
        }                        
    },
    methods: {
      //Максимумы по исполнению и просрочкам (туда вписываются остальные диаграммы)
      //Соотношение диаграм считается из суммы максимумов
      //и потом масштабируется исходя из допустимой суммарной ширины блоков
      //по умолчанию 80%  
      getDivsSizes:function (){
        var maxWidthRatio = this.orderInfo.totalDivsSize / (this.orderInfo.maxProgress + this.orderInfo.maxExpiration);
        var progressWidth = (this.orderInfo.maxProgress * maxWidthRatio) +'%';
        var expirationWidth = (this.orderInfo.maxExpiration * maxWidthRatio) +'%';
        return {progressWidth, expirationWidth};
      }    
    },
    template: `
          <div class="info">
            <div class="numbers">{{index}}</div>
            <div class="max_prg" :style="getMaxProgressStyle">
              <div :style="getNormalProgressStyle">|
                <div :style="getProgressStyle">{{orderInfo.daysInProgress}}</div>
                <div :style="getBeforeDueDays" v-if="beforeDueDays > 0">
                  {{beforeDueDays}}
                </div>
              </div>
            </div>
            <div class="max_exp" :style="getMaxExpirationStyle">|
              <div :style="getExpirationStyle" v-if="orderInfo.daysOfExpiration > 0">{{orderInfo.daysOfExpiration}}</div>
            </div>
            <div class="add_info">
            <div style="float: left; clear: both">
              <a :href="orderInfo.shortUrl" target="_blank">card</a>
            </div>
            <div style="float: left">{{orderInfo.name}}</div>
          </div>
          </div>
    `
  })
  