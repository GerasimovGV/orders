var app = new Vue({
  el: '#app',
  data: {
    message: 'Состояние заказов',
    orders: []//массив заказов
  },
  methods: {
    getOrdersInfo: function (event) {
      var data = null;
      self = this;
      //1) Получил ID доски
        var o = GetBoardID(trelloOptions);
        /*this.value = */trelloOptions.board.id =  o.id;
        //this.text = JSON.stringify(o, "", 2);//какая-то общая инфа о все карте вообще без подробностей
        //2) Получу список списков LISTS )"To Do", "Doing" , "Done")
        o = GetAllLists(trelloOptions);
        //this.text = JSON.stringify(o, "", 2);
        trelloOptions.lists.a = o;
        //2) получаю все имеющиеся на доске карты (CARDS)
        o = GetAllCards(trelloOptions);
        //3) А мне надо получить карты из конкретных списков 
        var a = GetCardsOfList (GetListIDByName('Все задачи крупных проектов', trelloOptions.lists.a) ,trelloOptions);
        var b = GetCardsOfList (GetListIDByName('Готовность к сборке', trelloOptions.lists.a) ,trelloOptions);
        var c = GetCardsOfList (GetListIDByName('Задания', trelloOptions.lists.a) ,trelloOptions);
        o = a.concat(b,c);
        //var o= GetCardsOfList (GetListIDByName('Задания', trelloOptions.lists.a) ,trelloOptions);
        //4) Карты из списка получил. Теперь приготовлю массив
        //   1. дата размещения карты (закодирован в ID карты)
        //   2. срок исполнения "due"
        //   3. Просрочка (значение с Минусом) или Сколько осталось (Значение с Плюсом)
        GetDatesTimesOfCards(o);
        //this.text = JSON.stringify(o, "", 2);//инфа о всех картах в Списке
        //в "o" массив объектов
        //5) найти максимальную просрочку для заполнения поля maxExpiration
        //   найти максимальный срок исполнения для заполнения поля maxProgress
        var xValues = o.map(function(o) { return o.daysOfExpiration; });
        var expMax = Math.max.apply(null, xValues);
            xValues = o.map(function(o) { return o.daysOfNormalProgress; });
        var prgMax = Math.max.apply(null, xValues);
        //6) добавить данные от макс просрочке и выполнении в массив
        let i = o.length;
        while (i-- != 0) {
          o[i].totalDivsSize = 40;
          o[i].maxExpiration = expMax;
          o[i].maxProgress = prgMax;
        }
        //7) отсортировать по максимальной просрочке
        o.sort(function (a, b) {
          if (a.daysOfExpiration < b.daysOfExpiration) { return  1;}
          if (a.daysOfExpiration > b.daysOfExpiration) { return -1;}
          return 0; //a = b
        });
        
        this.orders = o;
    }
  },
  template:`
  <div>
    <button v-on:click="getOrdersInfo">Get Orders Data</button>
      <process-diagram
        v-for="(order, index) in orders" :key="order.id"
        :orderInfo = order
        :index = index>
      </process-diagram>
  </div>
  `
})

/*
  <div>
    <button v-on:click="getOrdersInfo">Get Orders Data</button>
    <ol>
      <li v-for="order in orders">
        <process-diagram
          v-bind:orderInfo = order>
        </process-diagram>
      </li>
    </ol>
  </div>
  `
*/